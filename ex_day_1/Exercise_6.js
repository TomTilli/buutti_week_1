const str = "r1121212121212121212 ";

console.log(`This is how I want to see your word written as: ${str.trim().substring(0,20).toLowerCase()}`);


str.charCodeAt(0) >= 65 && str.charCodeAt(0) <= 90 //Testing for the capital letter in A-Z
|| str.charCodeAt(0) === 196 || str.charCodeAt(0) === 197 || str.charCodeAt(0) ===  214 //Testing for the capital lettes Å,Ä and Ö
|| str.slice(0,1) === " " || str.slice(-1) === " " || str.length > 20  //Testing for the length and spaces
? console.log("O-oh, there is an error") : console.log("Acceptable");



//Some stuff I like to have here for possible future reference

//str.charAt(0) === str.charAt(0).toUpperCase()
//Testing for capital letter only works for letters and not numbers or other characters not able to be upper and lower case.
//str.charCodeAt(0) >= 65 && str.charCodeAt(0) <= 90
// Å = 197
// Ä = 196
// Ö = 214
