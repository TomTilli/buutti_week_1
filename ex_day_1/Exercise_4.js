// A
const playerCount = 3;

playerCount === 4 ? console.log("You can play!") : console.log("Get more friends or get rid of some.");

// B
const isStressed = true;
const hasIcecream = false;

!isStressed || hasIcecream ? console.log("Mark is happy! :)") : console.log("Mark is sad :(")

// C
const sunIsShining = true;
const isRaining = false;
const temperature = 20;

sunIsShining && !isRaining && temperature >= 20 ? console.log("It is a beach day") : console.log("For the love of god stay inside");

// D
const seeSuzy = false;
const seeDan = true;

(!seeSuzy && !seeDan) || (seeSuzy && seeDan) ? console.log("Arin is sad! :(") : console.log("Arin is happy :)");