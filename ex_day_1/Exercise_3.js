//Strings
const str1 = "Lohi";
const str2 = "Käärme";

//Strings combined
const str_sum = str1 + str2.toLowerCase();

console.log(str_sum);

//String lengths
const str1_len = str1.length;
const str2_len = str2.length;

console.log(`The lengths of the words are ${str1_len} and ${str2_len}.`);

//Average String length
const str_avg = (str1_len + str2_len) / 2;

console.log(`The average of the lengths is ${str_avg}.\n`);

//Comparing lengths
str1_len < str_avg ? console.log(str1_len) : console.log(`${str1_len} is not smaller than ${str_avg}.`);
str2_len < str_avg ? console.log(str2_len) : console.log(`${str1_len} is not smaller than ${str_avg}.`);