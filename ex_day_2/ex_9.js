
const str = process.argv[2];

//split string to array
const str_arr = str.split(" ");
//remove last element from array
const removed_str = str_arr.slice(0,-1);
//join array back to string with spaces in between
console.log(removed_str.join(" "));