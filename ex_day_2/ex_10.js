const char_1 = process.argv[2]
const char_2 = process.argv[3]
const str = process.argv[4]

const replaced_char = new RegExp(char_1, "gi");

console.log(str.replace(replaced_char, char_2));
