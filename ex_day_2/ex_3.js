const numbers = [process.argv[2], process.argv[3], process.argv[4]]


if(numbers.every( (val, i, arr) => val === arr[0] )){
    console.log("All given numbers are equal.");
}else{ 
    console.log(`The largest given number is ${Math.max(...numbers)}.`);
    console.log(`The smallest given number is ${Math.min(...numbers)}`);
};
