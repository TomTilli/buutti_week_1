let language = process.argv[2];

language = language || "en";

if(language.toLowerCase() === "en"){
    console.log("Hello World");
}else if(language.toLowerCase() === "fi"){
    console.log("Hei maailma");
}else if(language.toLowerCase() === "ara"){
    console.log("Marhaban bialealam");
}else{
    console.log("I'm sorry but that is not a recognized language");
};
