const balance = 100;
const isActive = true;
const checkBalance = false;

if(checkBalance === true){ //do you want to check balance
    if(isActive === true && balance > 0){ //is your account active and have money in it
        console.log(`Your balance: ${balance}`);
    }else{
        if(isActive === false){ //is your account active
            console.log("Your account is not active.");
        }else{
            if(balance === 0){ //is there 0 money on your account
                console.log("Your account is empty.");
            }else{ //there is negative money in your account
                console.log("Your balance is negative.")
            };
        };
    };
}else{
    console.log("Have a nice day");
};