const num_1 = process.argv[2];
const num_2 = process.argv[3];
const password = process.argv[4] + " " + process.argv[5];

if(num_1 === num_2 && password.toLowerCase() === "hello world"){
    console.log("yay, you guessed the password");
}else if(num_1 === num_2){
    console.log("They are equal");
}else if(num_1 > num_2){
    console.log(`${num_1} is greater`);
}else{ //last possible outcome is for num_2 to be greater
    console.log(`${num_2} is greater`);
};
