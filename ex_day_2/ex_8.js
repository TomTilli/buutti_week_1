const order = process.argv[2];
const str = process.argv[3];

if(order.toLowerCase() === "lower"){
    console.log(str.toLowerCase());
}else if(order.toLowerCase() === "upper"){
    console.log(str.toUpperCase());
}else{
    console.log("Invalid input");
};